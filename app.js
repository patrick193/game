const STEP = 7;
let LEFTPRESS = false;
let RIGHTPRESS = false;
let PANAEL_X = 0;
let BRICKS = [];

class Paint {
  constructor(obj) {
    this.canvas = document.getElementById(obj.id);
    this.ctx = this.canvas.getContext("2d");
    this.x = obj.x || this.canvas.width/2;
    this.y = obj.y || this.canvas.height-30;
  }

  getCtx(canvas) {
    if (canvas) {
      return canvas.getContext("2d");
    }

    return this.ctx;
  }

  draw(radius = 0, color = "#03a529"){
    this.ctx.beginPath();
    this.ctx.arc(this.x, this.y, radius, 0, Math.PI*2);
    this.ctx.fillStyle = color;
    this.ctx.fill();
    this.ctx.closePath();
  }

  clear() {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }

  static independDraw(ctx, width, x, y) {
    // ctx.clearRect(0,0,)
    ctx.beginPath();
    ctx.rect(x, y, width,  10);
    ctx.fillStyle = "#03a529";
    ctx.fill();
    ctx.closePath();
  }
}

class Balls extends Paint{
  constructor(obj) {
    super(obj);
    this.radius = obj.radius;
    this.dx = obj.dx;
    this.dy = obj.dy;
  }

  generate(count = 1) {
    let ret = [];
    for (let i = 0; i < count; i++) {
      ret.push(new this(10, 2, 2))
    }

    return ret;
  }

  drawBall() {
    this.clear();
    super.draw(this.radius);
    if (this.x + this.dx > this.canvas.width - this.radius || this.x + this.dx < this.radius) {
      this.dx = -this.dx;
    }
    //if (this.y + this.dy > this.canvas.height - this.radius || this.y + this.dy < this.radius) {
      // this.dy = -this.dy;
    // } //dev mode
    if (this.y + this.dy < this.radius) {
      this.dy = -this.dy;
    }else if (this.y + this.dy > this.canvas.height - this.radius) {
      if (this.x > PANAEL_X && this.x < PANAEL_X + 70) {
        this.dy = -this.dy;
      }else {
          let score = new Score();
          score.show()
      }
    }
    this.x += this.dx;
    this.y += this.dy;
  }

  detect() {
    for (var i = 0; i < BRICKS.length; i++) {
      for (var j = 0; j < BRICKS[i].length; j++) {
        let brick = BRICKS[i][j];
        if (brick.status === 0) {
          if (this.x > brick.x && this.x < brick.width+brick.x && this.y < brick.height + brick.y) {
            this.dy = -this.dy;
            BRICKS[i][j].status = 1;

          }
        }
      }
    }
  }
}

class Panel {
  constructor() {
    this.rightPress = false;
    this.leftPress = false;
    this.panelWidth = 70;
  }
  drawPanel(ctx, canvas) {
    let x = PANAEL_X == 0 ? canvas.width/2 : PANAEL_X;
    let y = canvas.height -10;
    if (RIGHTPRESS == true && x < canvas.width - this.panelWidth) {
      x += STEP;
    }
    if (LEFTPRESS == true && x > 0) {
      x -= STEP;
    }
    PANAEL_X = x;
    Paint.independDraw(ctx, this.panelWidth, x,y);
  }
  setEvents() {
    document.addEventListener("keydown", keyDownHandler, false);
    document.addEventListener("keyup", keyUpHandler, false);
    let rp, lp = false;
    this.rightPress = rp;
    this.leftPress = lp;
    function keyDownHandler(e) {
      if (e.keyCode == 39) {
        RIGHTPRESS = true;
      } else if(e.keyCode == 37) {
        LEFTPRESS = true;
      }
    }
    function keyUpHandler(e) {
      if (e.keyCode == 39) {
        RIGHTPRESS = false;
      } else if(e.keyCode == 37) {
        LEFTPRESS = false;
      }
    }
  }
}

class Bricks {
  constructor() {
    this.raw = 20;
    this.column = 21;
    this.width = 75;
    this.height = 10;
    this.bricks = [];
  }
  generate(ctx, ballX, ballY) {
    for (var i = 0; i < this.column; i++) {
      BRICKS[i] = BRICKS[i] || [];
      for (var j = 0; j < this.raw; j++) {
        let status = BRICKS[i] && BRICKS[i][j] ? BRICKS[i][j].status :0;
        let brick = BRICKS[i][j] || {x: (i * (this.width+this.height)) + this.height, y:(j*(this.height+this.height)+this.height), status: status, width: this.width, height: this.height};
        BRICKS[i][j] = brick;
        if (BRICKS[i][j].status === 0) {
          this.drawBricks(ctx, brick)
        } else if (BRICKS[i][j].status === 1) {
          if (BRICKS[i][j].y < 800-BRICKS[i][j].height) {
              BRICKS[i][j].y += 2
              BRICKS[i][j].color = "#091df2"
              this.drawBricks(ctx, brick);
          }
        }
      }
  }
}
  drawBricks(ctx, brick) {
          ctx.beginPath();
          ctx.rect(brick.x, brick.y, 75, 10);
          ctx.fillStyle = brick.color||"#03a529";
          ctx.fill();
          ctx.closePath();
  }
}

class Score {
  constructor() {
    this.score = 0;
  }
  count() {
    for (var i = 0; i < BRICKS.length; i++) {
      for (var j = 0; j < BRICKS[i].length; j++) {
        if (BRICKS[i][j].status === 1){
          this.score++;
        }
      }
    }

    return this.score;
  }

  show() {
    this.count();
    alert("Score: " + this.score);
    document.location.reload();

  }
}
window.onload = () => {
  let balls = null;
  let panel = null;
  let bricks = null;
  let scoreElement = null;
  let score = null
  setInterval(() => {
    if (!balls)  {
      balls = new Balls({id: 'ball', radius: 10, dx: 2, dy: -2})
    }
    if (!panel) {
      panel = new Panel();
    }
    if (!bricks) {
      bricks = new Bricks();
    }
    balls.drawBall();
    panel.setEvents();
    balls.detect();
    panel.drawPanel(balls.ctx, balls.canvas);
    bricks.generate(balls.ctx);
  }, 10);
}
